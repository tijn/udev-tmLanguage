# udev.tmLanguage

Syntax highlighting for udev rules.

## Installation

Install via [Package Control](https://packagecontrol.io/packages/udev%20rules).

(or copy the whole thing into a directory in ~/.config/sublime-text-3/Packages)
